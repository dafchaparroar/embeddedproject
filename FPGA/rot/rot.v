module contador(input clk, output  data);
wire clk;
reg [25:0] data_ = 26'b00000000000000000000000000;

always @(posedge clk) begin
  data_ <= data_ + 1;
end
assign data = data_[25];
endmodule
