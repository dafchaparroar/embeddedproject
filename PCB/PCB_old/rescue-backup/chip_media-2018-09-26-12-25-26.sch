EESchema Schematic File Version 2
LIBS:chip_media-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:chip_pro
LIBS:stm32f100vxx
LIBS:ABM8G
LIBS:drv8835
LIBS:lt1117cst
LIBS:stm32f0
LIBS:Carlolib-dev
LIBS:opendous
LIBS:mem_mic
LIBS:usba-plug
LIBS:imx233stamp-cache
LIBS:srf2012
LIBS:rclamp0502b
LIBS:usbconn
LIBS:usb_a
LIBS:chip_media-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1900 2875 1825 2025
U 5844D827
F0 "stm32" 60
F1 "stm32.sch" 60
F2 "SAM_RX" O R 3725 4025 60 
F3 "SAM_TX" O R 3725 4125 60 
F4 "JNRST" B R 3725 3300 60 
F5 "JTMS" B R 3725 3400 60 
F6 "JTCK" B R 3725 3500 60 
F7 "JTDO" B R 3725 3700 60 
F8 "JTDI" B R 3725 3600 60 
F9 "JTRST" B R 3725 3800 60 
F10 "VM" I L 1900 3550 60 
F11 "BOOT0" B R 3725 3900 60 
F12 "SDA" I L 1900 4175 60 
F13 "SCL" I L 1900 4275 60 
$EndSheet
$Comp
L chip_pro U1
U 1 1 5844E7A8
P 6125 3650
F 0 "U1" H 5150 4825 60  0000 C CNN
F 1 "chip_pro" H 5300 4725 60  0000 C CNN
F 2 "CHIP_Pro-Footprint:CHIP_PRO" H 6125 3650 60  0001 C CNN
F 3 "" H 6125 3650 60  0001 C CNN
	1    6125 3650
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 584632AC
P 4500 2900
F 0 "#PWR01" H 4500 2900 30  0001 C CNN
F 1 "GND" H 4500 2830 30  0001 C CNN
F 2 "" H 4500 2900 60  0001 C CNN
F 3 "" H 4500 2900 60  0001 C CNN
	1    4500 2900
	0    1    -1   0   
$EndComp
$Comp
L GND #PWR02
U 1 1 58463508
P 7700 2900
F 0 "#PWR02" H 7700 2900 30  0001 C CNN
F 1 "GND" H 7700 2830 30  0001 C CNN
F 2 "" H 7700 2900 60  0001 C CNN
F 3 "" H 7700 2900 60  0001 C CNN
	1    7700 2900
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR03
U 1 1 58463B2C
P 5775 1800
F 0 "#PWR03" H 5775 1800 30  0001 C CNN
F 1 "GND" H 5775 1730 30  0001 C CNN
F 2 "" H 5775 1800 60  0001 C CNN
F 3 "" H 5775 1800 60  0001 C CNN
	1    5775 1800
	1    0    0    1   
$EndComp
$Comp
L GND #PWR04
U 1 1 58463C48
P 6475 1800
F 0 "#PWR04" H 6475 1800 30  0001 C CNN
F 1 "GND" H 6475 1730 30  0001 C CNN
F 2 "" H 6475 1800 60  0001 C CNN
F 3 "" H 6475 1800 60  0001 C CNN
	1    6475 1800
	1    0    0    1   
$EndComp
$Comp
L GND #PWR05
U 1 1 5846469F
P 6075 1800
F 0 "#PWR05" H 6075 1800 30  0001 C CNN
F 1 "GND" H 6075 1730 30  0001 C CNN
F 2 "" H 6075 1800 60  0001 C CNN
F 3 "" H 6075 1800 60  0001 C CNN
	1    6075 1800
	1    0    0    1   
$EndComp
$Comp
L GND #PWR06
U 1 1 584648A4
P 7900 3400
F 0 "#PWR06" H 7900 3400 30  0001 C CNN
F 1 "GND" H 7900 3330 30  0001 C CNN
F 2 "" H 7900 3400 60  0001 C CNN
F 3 "" H 7900 3400 60  0001 C CNN
	1    7900 3400
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR07
U 1 1 5846538C
P 6725 5225
F 0 "#PWR07" H 6725 5225 30  0001 C CNN
F 1 "GND" H 6725 5155 30  0001 C CNN
F 2 "" H 6725 5225 60  0001 C CNN
F 3 "" H 6725 5225 60  0001 C CNN
	1    6725 5225
	-1   0    0    -1  
$EndComp
Text Label 3750 4125 0    60   ~ 0
SAM_TX
Text Label 3750 4025 0    60   ~ 0
SAM_RX
$Comp
L +5V #PWR08
U 1 1 58475B6A
P 8125 2900
F 0 "#PWR08" H 8125 2750 50  0001 C CNN
F 1 "+5V" H 8125 3050 50  0000 C CNN
F 2 "" H 8125 2900 50  0000 C CNN
F 3 "" H 8125 2900 50  0000 C CNN
	1    8125 2900
	-1   0    0    -1  
$EndComp
$Comp
L SW_PUSH PW1
U 1 1 5847760F
P 3025 5375
F 0 "PW1" H 3175 5485 50  0000 C CNN
F 1 "SW_PUSH" H 3025 5295 50  0000 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_Tactile_SPST_Angled" H 3025 5375 60  0001 C CNN
F 3 "" H 3025 5375 60  0000 C CNN
	1    3025 5375
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 5847775B
P 3575 5450
F 0 "#PWR09" H 3575 5450 30  0001 C CNN
F 1 "GND" H 3575 5380 30  0001 C CNN
F 2 "" H 3575 5450 60  0001 C CNN
F 3 "" H 3575 5450 60  0001 C CNN
	1    3575 5450
	1    0    0    -1  
$EndComp
Text Label 2375 5375 0    60   ~ 0
PWR_ON
Text Label 7975 3300 2    60   ~ 0
PWR_ON
Text Label 7725 3500 2    60   ~ 0
TS
Text Label 7775 3600 2    60   ~ 0
BAT
$Comp
L STEREO-AUDIO-JACK_SJ1-3524-SMT HP1
U 1 1 58464B08
P 900 7025
F 0 "HP1" H 650 6675 40  0000 C CNN
F 1 "Headphones" H 825 7350 30  0000 C CNN
F 2 "SJ-3524-SMT_Stereo_Jack" H 900 7025 60  0001 C CNN
F 3 "" H 900 7025 60  0000 C CNN
	1    900  7025
	1    0    0    -1  
$EndComp
$Comp
L C CE1
U 1 1 58464B16
P 1950 7475
F 0 "CE1" H 2000 7575 50  0000 L CNN
F 1 "2.2nF" H 2000 7375 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 1950 7475 60  0001 C CNN
F 3 "" H 1950 7475 60  0001 C CNN
	1    1950 7475
	1    0    0    -1  
$EndComp
NoConn ~ 1200 7175
Text Label 1700 6775 0    60   ~ 0
HP_VGND
Text Label 1750 6925 0    60   ~ 0
HP_R
Text Label 1750 7275 0    60   ~ 0
HP_L
$Comp
L C CE2
U 1 1 58464B2E
P 2200 7475
F 0 "CE2" H 2250 7575 50  0000 L CNN
F 1 "2.2nF" H 2250 7375 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 2200 7475 60  0001 C CNN
F 3 "" H 2200 7475 60  0001 C CNN
	1    2200 7475
	1    0    0    -1  
$EndComp
$Comp
L L LA1
U 1 1 58464B81
P 1500 6775
F 0 "LA1" V 1550 6950 50  0000 C CNN
F 1 "LST1" V 1600 6600 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 1598 6775 50  0001 C CNN
F 3 "" H 1500 6775 50  0000 C CNN
	1    1500 6775
	0    -1   -1   0   
$EndComp
$Comp
L L LA2
U 1 1 58464B88
P 1500 6925
F 0 "LA2" V 1550 7100 50  0000 C CNN
F 1 "LST1" V 1550 6750 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 1598 6925 50  0001 C CNN
F 3 "" H 1500 6925 50  0000 C CNN
	1    1500 6925
	0    -1   -1   0   
$EndComp
$Comp
L L LA3
U 1 1 58464B8F
P 1500 7275
F 0 "LA3" V 1550 7475 50  0000 C CNN
F 1 "LST1" V 1575 7125 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 1598 7275 50  0001 C CNN
F 3 "" H 1500 7275 50  0000 C CNN
	1    1500 7275
	0    -1   -1   0   
$EndComp
Text Label 6525 5500 1    60   ~ 0
HP_VGND
Text Label 6425 5425 1    60   ~ 0
HP_R
Text Label 6625 5450 1    60   ~ 0
HP_L
$Comp
L CONN_01X06 ECOM1
U 1 1 5848971C
P 1175 4325
F 0 "ECOM1" V 1300 4300 50  0000 C CNN
F 1 "CONN_01X06" H 1253 4275 50  0001 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x06" H 1175 4325 50  0001 C CNN
F 3 "" H 1175 4325 50  0000 C CNN
	1    1175 4325
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR010
U 1 1 5848BA42
P 1725 4375
F 0 "#PWR010" H 1725 4375 30  0001 C CNN
F 1 "GND" H 1725 4305 30  0001 C CNN
F 2 "" H 1725 4375 60  0001 C CNN
F 3 "" H 1725 4375 60  0001 C CNN
	1    1725 4375
	1    0    0    -1  
$EndComp
$Comp
L +3.3VP #PWR011
U 1 1 5848C3B2
P 1450 3975
F 0 "#PWR011" H 1500 4005 20  0001 C CNN
F 1 "+3.3VP" H 1450 4065 30  0000 C CNN
F 2 "" H 1450 3975 60  0001 C CNN
F 3 "" H 1450 3975 60  0001 C CNN
	1    1450 3975
	1    0    0    -1  
$EndComp
Text Label 1375 4475 0    60   ~ 0
G8_TX
Text Label 1375 4575 0    60   ~ 0
G8_RX
Text Label 1475 4175 0    60   ~ 0
SDA
Text Label 1475 4275 0    60   ~ 0
SCL
Text Label 7625 3900 0    60   ~ 0
SDA
Text Label 7625 4000 0    60   ~ 0
SCL
$Comp
L L LSEL3
U 1 1 58496B21
P 7775 3000
F 0 "LSEL3" V 7725 3000 50  0000 C CNN
F 1 "LST1" V 7725 3000 50  0001 C CNN
F 2 "Resistors_SMD:R_0805" V 7873 3000 50  0001 C CNN
F 3 "" H 7775 3000 50  0000 C CNN
	1    7775 3000
	0    -1   -1   0   
$EndComp
$Comp
L +3.3VP #PWR012
U 1 1 58497735
P 7975 2875
F 0 "#PWR012" H 8025 2905 20  0001 C CNN
F 1 "+3.3VP" H 7925 3000 30  0000 C CNN
F 2 "" H 7975 2875 60  0001 C CNN
F 3 "" H 7975 2875 60  0001 C CNN
	1    7975 2875
	1    0    0    -1  
$EndComp
$Comp
L R RGR8
U 1 1 5849C971
P 8250 4400
F 0 "RGR8" V 8330 4400 50  0000 C CNN
F 1 "1k" V 8250 4400 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 8250 4400 60  0001 C CNN
F 3 "" H 8250 4400 60  0001 C CNN
	1    8250 4400
	0    -1   1    0   
$EndComp
$Comp
L LED DRGR8
U 1 1 5849C978
P 7900 4400
F 0 "DRGR8" H 7725 4475 50  0000 C CNN
F 1 "LED" H 8050 4475 50  0000 C CNN
F 2 "LEDs:LED_0603" H 7900 4400 60  0001 C CNN
F 3 "" H 7900 4400 60  0001 C CNN
	1    7900 4400
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR013
U 1 1 5849C97F
P 8475 4400
F 0 "#PWR013" H 8475 4400 30  0001 C CNN
F 1 "GND" H 8475 4330 30  0001 C CNN
F 2 "" H 8475 4400 60  0001 C CNN
F 3 "" H 8475 4400 60  0001 C CNN
	1    8475 4400
	0    -1   -1   0   
$EndComp
NoConn ~ 7625 3700
NoConn ~ 7625 3800
NoConn ~ 7625 4300
NoConn ~ 6325 5150
NoConn ~ 6225 5150
NoConn ~ 6125 5150
NoConn ~ 6025 5150
NoConn ~ 5925 5150
NoConn ~ 4775 3200
Text Label 2300 7675 0    60   ~ 0
AGND
Text Label 5825 5300 0    60   ~ 0
AGND
$Comp
L C CM2
U 1 1 58701499
P 2775 6850
F 0 "CM2" H 2825 6950 50  0000 L CNN
F 1 ".1uF" H 2825 6750 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 2775 6850 60  0001 C CNN
F 3 "" H 2775 6850 60  0001 C CNN
	1    2775 6850
	0    1    1    0   
$EndComp
$Comp
L C CM1
U 1 1 58701889
P 3075 7225
F 0 "CM1" H 3125 7325 50  0000 L CNN
F 1 ".1uF" H 3125 7125 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3075 7225 60  0001 C CNN
F 3 "" H 3075 7225 60  0001 C CNN
	1    3075 7225
	1    0    0    -1  
$EndComp
$Comp
L C CM3
U 1 1 58701991
P 4300 6475
F 0 "CM3" H 4350 6575 50  0000 L CNN
F 1 ".1uF" H 4350 6375 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4300 6475 60  0001 C CNN
F 3 "" H 4300 6475 60  0001 C CNN
	1    4300 6475
	-1   0    0    1   
$EndComp
$Comp
L MEM_MIC MIC1
U 1 1 58702594
P 3650 6850
F 0 "MIC1" H 3877 6825 60  0000 L CNN
F 1 "MEM_MIC" H 3877 6772 60  0001 L CNN
F 2 "modules:SPW2430HR5H-B" H 3650 6850 60  0001 C CNN
F 3 "" H 3650 6850 60  0001 C CNN
	1    3650 6850
	1    0    0    -1  
$EndComp
Text Label 3800 7675 0    60   ~ 0
AGND
Text Label 5225 5250 0    60   ~ 0
VMIC
Text Label 3425 6250 0    60   ~ 0
VMIC
Text Label 5225 5350 0    60   ~ 0
MICIN1
Text Label 5225 5450 0    60   ~ 0
MICIN2
Text Label 4300 6950 1    60   ~ 0
AGND
Text Label 2300 6850 0    60   ~ 0
MICIN1
Text Notes 8300 3025 0    60   ~ 0
Mount LSEL2 or LSEL3 not both
$Comp
L DC_POWER_JACK PWR1
U 1 1 587139AE
P 875 3300
F 0 "PWR1" H 832 3515 40  0000 C CNN
F 1 "DC_POWER_JACK" H 832 3503 25  0001 C CNN
F 2 "Connect:BARREL_JACK" H 875 3300 60  0001 C CNN
F 3 "" H 875 3300 60  0001 C CNN
	1    875  3300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR014
U 1 1 5871470F
P 1150 3400
F 0 "#PWR014" H 1150 3400 30  0001 C CNN
F 1 "GND" H 1150 3330 30  0001 C CNN
F 2 "" H 1150 3400 60  0001 C CNN
F 3 "" H 1150 3400 60  0001 C CNN
	1    1150 3400
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR015
U 1 1 58714E8F
P 1100 3200
F 0 "#PWR015" H 1100 3050 50  0001 C CNN
F 1 "+5V" H 1100 3350 50  0000 C CNN
F 2 "" H 1100 3200 50  0000 C CNN
F 3 "" H 1100 3200 50  0000 C CNN
	1    1100 3200
	-1   0    0    -1  
$EndComp
Text Label 7625 4100 0    60   ~ 0
G8_TX
Text Label 7625 4200 0    60   ~ 0
G8_RX
$Sheet
S 9450 3550 725  600 
U 58715936
F0 "USB" 60
F1 "USB.sch" 60
F2 "USB_DM0" I L 9450 3675 60 
F3 "USB_DP0" I L 9450 3775 60 
F4 "USB_DM1" I L 9450 3925 60 
F5 "USB_DP2" I L 9450 4025 60 
$EndSheet
Text Label 6275 2000 1    60   ~ 0
USB_D0+
Text Label 6375 2000 1    60   ~ 0
USB_D0-
Text Label 5875 2000 1    60   ~ 0
USB_D1+
Text Label 5975 2000 1    60   ~ 0
USB_D1-
Text Label 9000 3775 0    60   ~ 0
USB_D0+
Text Label 9000 3675 0    60   ~ 0
USB_D0-
Text Label 9000 4025 0    60   ~ 0
USB_D1+
Text Label 9000 3925 0    60   ~ 0
USB_D1-
$Comp
L CONN_01X05 VM_SEL1
U 1 1 58726582
P 1075 5525
F 0 "VM_SEL1" V 1200 5500 50  0000 C CNN
F 1 "CONN_01X05" H 1153 5475 50  0001 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x05" H 1075 5525 50  0001 C CNN
F 3 "" H 1075 5525 50  0000 C CNN
	1    1075 5525
	-1   0    0    1   
$EndComp
Text Label 1725 3550 0    60   ~ 0
VM
Text Label 1325 5525 0    60   ~ 0
VM
$Comp
L GND #PWR016
U 1 1 587277CF
P 1450 5725
F 0 "#PWR016" H 1450 5725 30  0001 C CNN
F 1 "GND" H 1450 5655 30  0001 C CNN
F 2 "" H 1450 5725 60  0001 C CNN
F 3 "" H 1450 5725 60  0001 C CNN
	1    1450 5725
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR017
U 1 1 58728697
P 1575 5425
F 0 "#PWR017" H 1575 5275 50  0001 C CNN
F 1 "+5V" H 1575 5575 50  0000 C CNN
F 2 "" H 1575 5425 50  0000 C CNN
F 3 "" H 1575 5425 50  0000 C CNN
	1    1575 5425
	-1   0    0    -1  
$EndComp
Text Label 1275 5325 0    60   ~ 0
VEXT
$Comp
L CONN_01X05 TANG1
U 1 1 58A125E5
P 4100 5275
F 0 "TANG1" V 4225 5250 50  0000 C CNN
F 1 "CONN_01X05" H 4178 5225 50  0001 L CNN
F 2 "Socket_Strips:Socket_Strip_Angled_1x05_Pitch2.54mm" H 4100 5275 50  0001 C CNN
F 3 "" H 4100 5275 50  0000 C CNN
	1    4100 5275
	-1   0    0    1   
$EndComp
$Comp
L +3.3VP #PWR018
U 1 1 58A13994
P 4450 5000
F 0 "#PWR018" H 4500 5030 20  0001 C CNN
F 1 "+3.3VP" H 4400 5125 30  0000 C CNN
F 2 "" H 4450 5000 60  0001 C CNN
F 3 "" H 4450 5000 60  0001 C CNN
	1    4450 5000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR019
U 1 1 58A13FE2
P 4450 5525
F 0 "#PWR019" H 4450 5525 30  0001 C CNN
F 1 "GND" H 4450 5455 30  0001 C CNN
F 2 "" H 4450 5525 60  0001 C CNN
F 3 "" H 4450 5525 60  0001 C CNN
	1    4450 5525
	1    0    0    -1  
$EndComp
Text Label 4325 5175 0    60   ~ 0
DATA_T
Text Label 4325 5275 0    60   ~ 0
CLK_T
Text Label 4325 5375 0    60   ~ 0
PL_T
Text Label 4450 4000 0    60   ~ 0
DATA_T
Text Label 4450 4100 0    60   ~ 0
CLK_T
Text Label 4450 4200 0    60   ~ 0
PL_T
$Comp
L CONN_01X03 BATT1
U 1 1 58D5DC8B
P 8425 3600
F 0 "BATT1" V 8550 3575 50  0000 C CNN
F 1 "CONN_01X03" H 8503 3550 50  0001 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x03_Pitch2.54mm" H 8425 3600 50  0001 C CNN
F 3 "" H 8425 3600 50  0000 C CNN
	1    8425 3600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR020
U 1 1 58D5ECC4
P 8125 3750
F 0 "#PWR020" H 8125 3750 30  0001 C CNN
F 1 "GND" H 8125 3680 30  0001 C CNN
F 2 "" H 8125 3750 60  0001 C CNN
F 3 "" H 8125 3750 60  0001 C CNN
	1    8125 3750
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH PR1
U 1 1 58D87990
P 9450 5750
F 0 "PR1" H 9600 5860 50  0000 C CNN
F 1 "SW_PUSH" H 9450 5670 50  0000 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_Tactile_SPST_Angled" H 9450 5750 60  0001 C CNN
F 3 "" H 9450 5750 60  0000 C CNN
	1    9450 5750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR021
U 1 1 58D87996
P 10000 5825
F 0 "#PWR021" H 10000 5825 30  0001 C CNN
F 1 "GND" H 10000 5755 30  0001 C CNN
F 2 "" H 10000 5825 60  0001 C CNN
F 3 "" H 10000 5825 60  0001 C CNN
	1    10000 5825
	1    0    0    -1  
$EndComp
Text Label 8600 5750 0    60   ~ 0
RUN
$Comp
L R RR1
U 1 1 58D88F67
P 9075 5325
F 0 "RR1" V 9155 5325 50  0000 C CNN
F 1 "10k" V 9075 5325 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 9075 5325 60  0001 C CNN
F 3 "" H 9075 5325 60  0001 C CNN
	1    9075 5325
	-1   0    0    1   
$EndComp
$Comp
L +3.3VP #PWR022
U 1 1 58D89861
P 9075 5125
F 0 "#PWR022" H 9125 5155 20  0001 C CNN
F 1 "+3.3VP" H 9025 5250 30  0000 C CNN
F 2 "" H 9075 5125 60  0001 C CNN
F 3 "" H 9075 5125 60  0001 C CNN
	1    9075 5125
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 2900 4775 2900
Wire Wire Line
	7700 2900 7625 2900
Wire Wire Line
	6475 1800 6475 2000
Wire Wire Line
	5775 1800 5775 2000
Wire Wire Line
	6075 1800 6075 2000
Wire Wire Line
	7900 3400 7625 3400
Wire Wire Line
	6725 5150 6725 5225
Wire Wire Line
	5825 5150 5825 5300
Wire Wire Line
	1650 4475 1375 4475
Wire Wire Line
	1650 4575 1375 4575
Wire Wire Line
	3725 4025 4225 4025
Wire Wire Line
	4150 4125 3725 4125
Wire Wire Line
	3325 5375 3575 5375
Wire Wire Line
	3575 5375 3575 5450
Wire Wire Line
	2725 5375 2375 5375
Wire Wire Line
	7625 3300 7975 3300
Wire Wire Line
	7625 3500 8225 3500
Wire Wire Line
	7625 3600 8225 3600
Wire Wire Line
	1350 6775 1200 6775
Wire Wire Line
	1350 6925 1200 6925
Wire Wire Line
	1350 7275 1200 7275
Wire Wire Line
	2050 6775 1650 6775
Wire Wire Line
	1650 6925 2200 6925
Wire Wire Line
	1650 7275 1950 7275
Wire Wire Line
	2200 6925 2200 7325
Wire Wire Line
	1950 7675 2575 7675
Wire Wire Line
	1950 7625 1950 7675
Connection ~ 2200 7675
Wire Wire Line
	1950 7275 1950 7325
Wire Wire Line
	2200 7625 2200 7675
Connection ~ 1950 7675
Wire Wire Line
	6525 5150 6525 5550
Wire Wire Line
	6425 5475 6425 5150
Wire Wire Line
	6625 5150 6625 5500
Wire Wire Line
	1375 4175 1900 4175
Wire Wire Line
	1900 4275 1375 4275
Wire Wire Line
	1375 4375 1725 4375
Wire Wire Line
	1450 3975 1450 4075
Wire Wire Line
	1450 4075 1375 4075
Wire Wire Line
	3725 3300 4775 3300
Wire Wire Line
	3725 3400 4775 3400
Wire Wire Line
	3725 3500 4775 3500
Wire Wire Line
	3725 3600 4775 3600
Wire Wire Line
	3725 3700 4775 3700
Wire Wire Line
	3725 3800 4775 3800
Wire Wire Line
	3725 3900 4775 3900
Wire Wire Line
	7625 3900 7975 3900
Wire Wire Line
	7625 4000 7975 4000
Wire Wire Line
	8125 2900 8125 3200
Wire Wire Line
	7975 2875 7975 3000
Wire Wire Line
	7975 3000 7925 3000
Wire Wire Line
	7975 4100 7625 4100
Wire Wire Line
	7975 4200 7625 4200
Wire Wire Line
	8475 4400 8400 4400
Wire Wire Line
	8100 4400 8050 4400
Wire Wire Line
	7625 4400 7750 4400
Wire Wire Line
	8125 3200 7625 3200
Connection ~ 8125 3100
Wire Wire Line
	3550 7675 3550 7500
Wire Wire Line
	3075 7675 4025 7675
Wire Wire Line
	3750 7675 3750 7500
Connection ~ 3550 7675
Connection ~ 3750 7675
Wire Wire Line
	3075 7375 3075 7675
Wire Wire Line
	2925 6850 3250 6850
Wire Wire Line
	3075 7075 3075 6850
Connection ~ 3075 6850
Wire Wire Line
	5525 5150 5525 5250
Wire Wire Line
	5525 5250 5225 5250
Wire Wire Line
	3425 6250 4300 6250
Wire Wire Line
	5625 5150 5625 5350
Wire Wire Line
	5625 5350 5225 5350
Wire Wire Line
	5725 5150 5725 5450
Wire Wire Line
	5725 5450 5225 5450
Wire Wire Line
	4300 6250 4300 6325
Connection ~ 3650 6250
Wire Wire Line
	4300 6625 4300 6975
Wire Wire Line
	2625 6850 2300 6850
Wire Wire Line
	1025 3300 1025 3400
Wire Wire Line
	1025 3400 1150 3400
Wire Wire Line
	1025 3200 1100 3200
Wire Wire Line
	4225 4025 4225 4300
Wire Wire Line
	4225 4300 4775 4300
Wire Wire Line
	4150 4125 4150 4400
Wire Wire Line
	4150 4400 4775 4400
Wire Wire Line
	6275 2000 6275 1650
Wire Wire Line
	6375 1650 6375 2000
Wire Wire Line
	5875 2000 5875 1650
Wire Wire Line
	5975 1650 5975 2000
Wire Wire Line
	9000 3775 9450 3775
Wire Wire Line
	9000 3675 9450 3675
Wire Wire Line
	9000 4025 9450 4025
Wire Wire Line
	9000 3925 9450 3925
Wire Wire Line
	1700 3550 1900 3550
Wire Wire Line
	1275 5525 1475 5525
Wire Wire Line
	1275 5625 1575 5625
Wire Wire Line
	1275 5725 1450 5725
Wire Wire Line
	1575 5625 1575 5425
Wire Wire Line
	1275 5425 1475 5425
Wire Wire Line
	1275 5325 1475 5325
Wire Wire Line
	1475 5325 1475 5425
Wire Wire Line
	4300 5075 4450 5075
Wire Wire Line
	4450 5075 4450 5000
Wire Wire Line
	4300 5475 4450 5475
Wire Wire Line
	4450 5475 4450 5525
Wire Wire Line
	4300 5175 4650 5175
Wire Wire Line
	4300 5275 4650 5275
Wire Wire Line
	4300 5375 4650 5375
Wire Wire Line
	4425 4000 4775 4000
Wire Wire Line
	4425 4100 4775 4100
Wire Wire Line
	4425 4200 4775 4200
Wire Wire Line
	8125 3750 8125 3700
Wire Wire Line
	8125 3700 8225 3700
Wire Wire Line
	9750 5750 10000 5750
Wire Wire Line
	10000 5750 10000 5825
Wire Wire Line
	9150 5750 8600 5750
Wire Wire Line
	7725 5100 7725 5025
Wire Wire Line
	9075 5125 9075 5175
Wire Wire Line
	9075 5475 9075 5750
Connection ~ 9075 5750
Wire Wire Line
	4775 3100 4475 3100
Text Label 4525 3100 0    60   ~ 0
RUN
$EndSCHEMATC
