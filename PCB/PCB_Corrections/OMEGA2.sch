EESchema Schematic File Version 4
LIBS:schematicProject-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	7100 2650 7600 2650
Wire Wire Line
	7600 2650 7600 2350
Wire Wire Line
	5600 2500 5550 2500
Wire Wire Line
	5550 2500 5550 2300
Wire Wire Line
	5450 2300 5550 2300
Wire Wire Line
	7100 2300 7100 2500
Wire Wire Line
	5450 2300 5450 2350
$Comp
L schematicProject-rescue:OMEGA_2-omega-dock-new U?
U 1 1 5BA21305
P 6350 3550
AR Path="/5B7D7C2A/5BA21305" Ref="U?"  Part="1" 
AR Path="/5BA21305" Ref="U?"  Part="1" 
AR Path="/5BA1DFB2/5BA21305" Ref="U5"  Part="1" 
F 0 "U5" H 6700 4850 60  0000 C CNN
F 1 "OMEGA_2" H 6150 4850 60  0000 C CNN
F 2 "OMEGA2:XCVR_OMEGA2" H 6350 4150 60  0001 C CNN
F 3 "" H 6350 4150 60  0001 C CNN
	1    6350 3550
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:LED-device D?
U 1 1 5BA2130C
P 5250 2650
AR Path="/5BA2130C" Ref="D?"  Part="1" 
AR Path="/5BA1DFB2/5BA2130C" Ref="D2"  Part="1" 
F 0 "D2" H 5241 2866 50  0000 C CNN
F 1 "LED" H 5241 2775 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 5250 2650 50  0001 C CNN
F 3 "~" H 5250 2650 50  0001 C CNN
	1    5250 2650
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:R-device R?
U 1 1 5BA21313
P 4850 2650
AR Path="/5BA21313" Ref="R?"  Part="1" 
AR Path="/5BA1DFB2/5BA21313" Ref="R3"  Part="1" 
F 0 "R3" V 4643 2650 50  0000 C CNN
F 1 "1K" V 4734 2650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4780 2650 50  0001 C CNN
F 3 "" H 4850 2650 50  0001 C CNN
	1    4850 2650
	0    1    1    0   
$EndComp
Wire Wire Line
	5600 2650 5400 2650
Wire Wire Line
	5100 2650 5000 2650
$Comp
L schematicProject-rescue:GND-omega-dock-new-cache #PWR?
U 1 1 5BA2131C
P 4600 2650
AR Path="/5BA2131C" Ref="#PWR?"  Part="1" 
AR Path="/5BA1DFB2/5BA2131C" Ref="#PWR0120"  Part="1" 
F 0 "#PWR0120" H 4600 2400 50  0001 C CNN
F 1 "GND" V 4605 2522 50  0000 R CNN
F 2 "" H 4600 2650 50  0001 C CNN
F 3 "" H 4600 2650 50  0001 C CNN
	1    4600 2650
	0    1    1    0   
$EndComp
Wire Wire Line
	4600 2650 4700 2650
Wire Wire Line
	7100 4450 7400 4450
Wire Wire Line
	7100 3400 7400 3400
Text Label 7400 3400 2    50   ~ 0
RESET
$Comp
L schematicProject-rescue:GND-omega-dock-new-cache #PWR?
U 1 1 5BA2133B
P 5450 2350
AR Path="/5BA2133B" Ref="#PWR?"  Part="1" 
AR Path="/5BA1DFB2/5BA2133B" Ref="#PWR0121"  Part="1" 
F 0 "#PWR0121" H 5450 2100 50  0001 C CNN
F 1 "GND" H 5455 2177 50  0000 C CNN
F 2 "" H 5450 2350 50  0001 C CNN
F 3 "" H 5450 2350 50  0001 C CNN
	1    5450 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3550 5300 3550
Wire Wire Line
	5600 3700 5300 3700
Wire Wire Line
	7100 4600 7400 4600
Wire Wire Line
	7100 4750 7400 4750
Wire Wire Line
	7100 2950 7400 2950
Text Label 3250 5800 0    50   ~ 0
RESET
$Comp
L schematicProject-rescue:R-device R?
U 1 1 5BA27ED4
P 3600 6000
AR Path="/5BA27ED4" Ref="R?"  Part="1" 
AR Path="/5BA1DFB2/5BA27ED4" Ref="R1"  Part="1" 
F 0 "R1" H 3670 6046 50  0000 L CNN
F 1 "3k" H 3670 5955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3530 6000 50  0001 C CNN
F 3 "" H 3600 6000 50  0001 C CNN
	1    3600 6000
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:GND-omega-dock-new-cache #PWR?
U 1 1 5BA27EDB
P 3600 6250
AR Path="/5BA27EDB" Ref="#PWR?"  Part="1" 
AR Path="/5BA1DFB2/5BA27EDB" Ref="#PWR0122"  Part="1" 
F 0 "#PWR0122" H 3600 6000 50  0001 C CNN
F 1 "GND" H 3605 6077 50  0000 C CNN
F 2 "" H 3600 6250 50  0001 C CNN
F 3 "" H 3600 6250 50  0001 C CNN
	1    3600 6250
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:R-device R?
U 1 1 5BA27EE1
P 4200 5650
AR Path="/5BA27EE1" Ref="R?"  Part="1" 
AR Path="/5BA1DFB2/5BA27EE1" Ref="R2"  Part="1" 
F 0 "R2" H 4130 5604 50  0000 R CNN
F 1 "1k" H 4130 5695 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4130 5650 50  0001 C CNN
F 3 "" H 4200 5650 50  0001 C CNN
	1    4200 5650
	-1   0    0    1   
$EndComp
$Comp
L Switch:SW_Push SW?
U 1 1 5BA27EE8
P 3850 5800
AR Path="/5BA27EE8" Ref="SW?"  Part="1" 
AR Path="/5BA1DFB2/5BA27EE8" Ref="SW1"  Part="1" 
F 0 "SW1" H 3850 6085 50  0000 C CNN
F 1 "SW_Push" H 3850 5994 50  0000 C CNN
F 2 "EVQPF008K:EVQPF008K" H 3850 6000 50  0001 C CNN
F 3 "" H 3850 6000 50  0001 C CNN
	1    3850 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 5400 4200 5500
Wire Wire Line
	3600 5800 3600 5850
Wire Wire Line
	3200 5800 3600 5800
Connection ~ 3600 5800
Wire Wire Line
	3600 5800 3650 5800
Wire Wire Line
	3600 6150 3600 6250
Wire Wire Line
	4050 5800 4200 5800
Text HLabel 5300 3550 0    50   Input ~ 0
ON1_RX
Text HLabel 5300 3700 0    50   Input ~ 0
ON1_TX
Text HLabel 5250 3850 0    50   Input ~ 0
ON_MISO
Text HLabel 5250 4000 0    50   Input ~ 0
ON_MOSI
Text HLabel 5250 4150 0    50   Input ~ 0
ONSPI_CLK
Wire Wire Line
	5600 2950 5300 2950
Wire Wire Line
	5600 3100 5300 3100
Wire Wire Line
	5600 3250 5300 3250
Wire Wire Line
	5600 3400 5300 3400
Text HLabel 5300 3400 0    50   Input ~ 0
JTMS
Text HLabel 5300 2950 0    50   Input ~ 0
JTCK
Text HLabel 5300 3250 0    50   Input ~ 0
JTDI
Text HLabel 5300 3100 0    50   Input ~ 0
JTDO
Wire Wire Line
	5600 4600 5250 4600
Text HLabel 5250 4450 0    50   Input ~ 0
JTRST
Text HLabel 5250 4600 0    50   Input ~ 0
BOOT0
Wire Wire Line
	7400 2800 7100 2800
Text HLabel 7400 4450 2    50   Input ~ 0
JNRST
Text HLabel 7400 4600 2    50   Input ~ 0
SCL
Text HLabel 7400 4750 2    50   Input ~ 0
SDA
$Comp
L schematicProject-rescue:GND-omega-dock-new-cache #PWR?
U 1 1 5B98DB10
P 7250 2350
AR Path="/5B98DB10" Ref="#PWR?"  Part="1" 
AR Path="/5BA1DFB2/5B98DB10" Ref="#PWR0123"  Part="1" 
F 0 "#PWR0123" H 7250 2100 50  0001 C CNN
F 1 "GND" H 7255 2177 50  0000 C CNN
F 2 "" H 7250 2350 50  0001 C CNN
F 3 "" H 7250 2350 50  0001 C CNN
	1    7250 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 2300 7250 2300
Wire Wire Line
	7250 2300 7250 2350
Text Label 7400 3100 2    50   ~ 0
ON_RX
$Comp
L schematicProject-rescue:Conn_01x04-Connector_Generic J4
U 1 1 5BC97364
P 8250 3150
F 0 "J4" H 8350 2950 50  0000 L CNN
F 1 "Conn_01x04" H 8330 3051 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 8250 3150 50  0001 C CNN
F 3 "~" H 8250 3150 50  0001 C CNN
	1    8250 3150
	1    0    0    -1  
$EndComp
Text Label 7400 3250 2    50   ~ 0
ON_TX
Wire Wire Line
	7100 3250 8050 3250
Wire Wire Line
	7750 3100 7750 3150
Wire Wire Line
	7750 3150 8050 3150
Wire Wire Line
	7100 3100 7750 3100
Wire Wire Line
	8050 3050 7950 3050
Wire Wire Line
	7950 3350 7950 3500
Wire Wire Line
	7950 3350 8050 3350
$Comp
L schematicProject-rescue:GND-power #PWR0124
U 1 1 5BC998FF
P 7950 3500
F 0 "#PWR0124" H 7950 3250 50  0001 C CNN
F 1 "GND" H 7955 3327 50  0000 C CNN
F 2 "" H 7950 3500 50  0001 C CNN
F 3 "" H 7950 3500 50  0001 C CNN
	1    7950 3500
	1    0    0    -1  
$EndComp
Text HLabel 4200 5400 1    50   Input ~ 0
ON_3V3
Text HLabel 7600 2350 1    50   Input Italic 10
ON_3V3
Text HLabel 5250 4300 0    50   Input ~ 0
ONSPI_CS
Text GLabel 7400 2800 2    50   Input ~ 0
D+
Text GLabel 7400 2950 2    50   Input ~ 0
D-
Wire Wire Line
	7950 2650 7600 2650
Wire Wire Line
	7950 2650 7950 3050
Connection ~ 7600 2650
$Comp
L Device:C C49
U 1 1 5BC415D2
P 8450 2850
F 0 "C49" H 8565 2896 50  0000 L CNN
F 1 "0.1uF" H 8500 2800 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8488 2700 50  0001 C CNN
F 3 "~" H 8450 2850 50  0001 C CNN
	1    8450 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C50
U 1 1 5BC416BA
P 8800 2850
F 0 "C50" H 8915 2896 50  0000 L CNN
F 1 "0.1uF" H 8915 2805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8838 2700 50  0001 C CNN
F 3 "~" H 8800 2850 50  0001 C CNN
	1    8800 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 2650 8450 2650
Wire Wire Line
	8800 2650 8800 2700
Connection ~ 7950 2650
Wire Wire Line
	8450 2700 8450 2650
Connection ~ 8450 2650
Wire Wire Line
	8450 2650 8800 2650
$Comp
L schematicProject-rescue:GND-power #PWR0106
U 1 1 5BC42D95
P 8450 3000
F 0 "#PWR0106" H 8450 2750 50  0001 C CNN
F 1 "GND" H 8455 2827 50  0000 C CNN
F 2 "" H 8450 3000 50  0001 C CNN
F 3 "" H 8450 3000 50  0001 C CNN
	1    8450 3000
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:GND-power #PWR0107
U 1 1 5BC42DB2
P 8800 3000
F 0 "#PWR0107" H 8800 2750 50  0001 C CNN
F 1 "GND" H 8805 2827 50  0000 C CNN
F 2 "" H 8800 3000 50  0001 C CNN
F 3 "" H 8800 3000 50  0001 C CNN
	1    8800 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4300 5550 4300
Wire Wire Line
	5600 4450 5250 4450
Wire Wire Line
	5600 3850 5550 3850
Wire Wire Line
	5550 4000 5600 4000
Wire Wire Line
	5600 4150 5550 4150
$Comp
L schematicProject-rescue:R-device R?
U 1 1 5C918B9A
P 5400 3850
AR Path="/5C918B9A" Ref="R?"  Part="1" 
AR Path="/5BA1DFB2/5C918B9A" Ref="R13"  Part="1" 
F 0 "R13" V 5193 3850 50  0000 C CNN
F 1 "100" V 5284 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5330 3850 50  0001 C CNN
F 3 "" H 5400 3850 50  0001 C CNN
	1    5400 3850
	0    1    1    0   
$EndComp
$Comp
L schematicProject-rescue:R-device R?
U 1 1 5C919B60
P 5400 4000
AR Path="/5C919B60" Ref="R?"  Part="1" 
AR Path="/5BA1DFB2/5C919B60" Ref="R17"  Part="1" 
F 0 "R17" V 5193 4000 50  0000 C CNN
F 1 "100" V 5284 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5330 4000 50  0001 C CNN
F 3 "" H 5400 4000 50  0001 C CNN
	1    5400 4000
	0    1    1    0   
$EndComp
$Comp
L schematicProject-rescue:R-device R?
U 1 1 5C919CE2
P 5400 4150
AR Path="/5C919CE2" Ref="R?"  Part="1" 
AR Path="/5BA1DFB2/5C919CE2" Ref="R18"  Part="1" 
F 0 "R18" V 5193 4150 50  0000 C CNN
F 1 "100" V 5284 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5330 4150 50  0001 C CNN
F 3 "" H 5400 4150 50  0001 C CNN
	1    5400 4150
	0    1    1    0   
$EndComp
$Comp
L schematicProject-rescue:R-device R?
U 1 1 5C919F64
P 5400 4300
AR Path="/5C919F64" Ref="R?"  Part="1" 
AR Path="/5BA1DFB2/5C919F64" Ref="R22"  Part="1" 
F 0 "R22" V 5193 4300 50  0000 C CNN
F 1 "100" V 5284 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5330 4300 50  0001 C CNN
F 3 "" H 5400 4300 50  0001 C CNN
	1    5400 4300
	0    1    1    0   
$EndComp
Text HLabel 5250 2800 0    50   Input ~ 0
fpgaRST
Wire Wire Line
	5600 2800 5250 2800
$EndSCHEMATC
