EESchema Schematic File Version 4
LIBS:schematicProject-cache
EELAYER 26 0
EELAYER END
$Descr A1 33110 23386
encoding utf-8
Sheet 4 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Entry Wire Line
	9600 4600 9700 4700
Entry Wire Line
	9600 4700 9700 4800
Entry Wire Line
	9600 4800 9700 4900
Entry Wire Line
	9600 4300 9700 4400
Entry Wire Line
	9600 4400 9700 4500
Entry Wire Line
	9600 4500 9700 4600
Entry Wire Line
	9600 4000 9700 4100
Entry Wire Line
	9600 4100 9700 4200
Entry Wire Line
	9600 4200 9700 4300
Entry Wire Line
	9600 5200 9700 5300
Entry Wire Line
	9600 5300 9700 5400
Entry Wire Line
	9600 5400 9700 5500
Entry Wire Line
	9600 4900 9700 5000
Entry Wire Line
	9600 5000 9700 5100
Entry Wire Line
	9600 5100 9700 5200
Wire Bus Line
	9450 3850 9600 3850
Wire Wire Line
	9700 4100 10050 4100
Wire Wire Line
	9700 4200 10050 4200
Wire Wire Line
	9700 4300 10050 4300
Wire Wire Line
	9700 4400 10050 4400
Wire Wire Line
	9700 4500 10050 4500
Wire Wire Line
	9700 4600 10050 4600
Wire Wire Line
	9700 4700 10050 4700
Wire Wire Line
	9700 4800 10050 4800
Wire Wire Line
	9700 4900 10050 4900
Wire Wire Line
	9700 5000 10050 5000
Wire Wire Line
	9700 5100 10050 5100
Wire Wire Line
	9700 5200 10050 5200
Wire Wire Line
	9700 5300 10050 5300
Wire Wire Line
	9700 5400 10050 5400
Wire Wire Line
	9700 5500 10050 5500
Entry Wire Line
	8750 5000 8850 5100
Entry Wire Line
	8750 5100 8850 5200
Entry Wire Line
	8750 5200 8850 5300
Entry Wire Line
	8750 4700 8850 4800
Entry Wire Line
	8750 4800 8850 4900
Entry Wire Line
	8750 4900 8850 5000
Entry Wire Line
	8750 4400 8850 4500
Entry Wire Line
	8750 4500 8850 4600
Entry Wire Line
	8750 4600 8850 4700
Wire Bus Line
	8600 4250 8750 4250
Wire Wire Line
	8850 4500 9200 4500
Wire Wire Line
	8850 4600 9200 4600
Wire Wire Line
	8850 4700 9200 4700
Wire Wire Line
	8850 4800 9200 4800
Wire Wire Line
	8850 4900 9200 4900
Wire Wire Line
	8850 5000 9200 5000
Wire Wire Line
	8850 5100 9200 5100
Wire Wire Line
	8850 5200 9200 5200
Wire Wire Line
	8850 5300 9200 5300
Text Label 10050 4100 2    50   ~ 0
FMC_D0
Text Label 10050 4200 2    50   ~ 0
FMC_D1
Text Label 10050 4300 2    50   ~ 0
FMC_D2
Text Label 10050 4400 2    50   ~ 0
FMC_D3
Text Label 10050 4500 2    50   ~ 0
FMC_D4
Text Label 10050 4600 2    50   ~ 0
FMC_D5
Text Label 10050 4700 2    50   ~ 0
FMC_D6
Text Label 10050 4800 2    50   ~ 0
FMC_D7
Text Label 10050 4900 2    50   ~ 0
FMC_D8
Text Label 10050 5000 2    50   ~ 0
FMC_D9
Text Label 10050 5100 2    50   ~ 0
FMC_D10
Text Label 10050 5200 2    50   ~ 0
FMC_D11
Text Label 10050 5300 2    50   ~ 0
FMC_D12
Text Label 10050 5400 2    50   ~ 0
FMC_D13
Text Label 10050 5500 2    50   ~ 0
FMC_D14
Entry Wire Line
	9600 5500 9700 5600
Wire Wire Line
	9700 5600 10050 5600
Text Label 10050 5600 2    50   ~ 0
FMC_D15
Text Label 9200 4500 2    50   ~ 0
FMC_A16
Text Label 9200 4600 2    50   ~ 0
FMC_A17
Text Label 9200 4700 2    50   ~ 0
FMC_A18
Text Label 9200 4800 2    50   ~ 0
FMC_A19
Text Label 9200 4900 2    50   ~ 0
FMC_A20
Text Label 9200 5000 2    50   ~ 0
FMC_A21
Text Label 9200 5100 2    50   ~ 0
FMC_A22
Text Label 9200 5200 2    50   ~ 0
FMC_A23
Text Label 9200 5300 2    50   ~ 0
FMC_A24
Text HLabel 9450 3850 0    50   BiDi ~ 0
FMC_D[15:0]
Text HLabel 8600 4250 0    50   Input ~ 0
FMC_A[24:16]
$Comp
L schematicProject-rescue:LED-device D?
U 1 1 5BCD4A05
P 9100 3100
AR Path="/5B95942F/5BCD4A05" Ref="D?"  Part="1" 
AR Path="/5BB736EF/5BCD4A05" Ref="D?"  Part="1" 
AR Path="/5BB72E68/5BCD4A05" Ref="D?"  Part="1" 
F 0 "D?" H 9092 2845 50  0000 C CNN
F 1 "LED" H 9092 2936 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 9100 3100 50  0001 C CNN
F 3 "~" H 9100 3100 50  0001 C CNN
	1    9100 3100
	-1   0    0    1   
$EndComp
$Comp
L schematicProject-rescue:R-device R?
U 1 1 5BCD4A0C
P 9450 3100
AR Path="/5B95942F/5BCD4A0C" Ref="R?"  Part="1" 
AR Path="/5BB736EF/5BCD4A0C" Ref="R?"  Part="1" 
AR Path="/5BB72E68/5BCD4A0C" Ref="R?"  Part="1" 
F 0 "R?" V 9243 3100 50  0000 C CNN
F 1 "1k" V 9334 3100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9380 3100 50  0001 C CNN
F 3 "" H 9450 3100 50  0001 C CNN
	1    9450 3100
	0    1    1    0   
$EndComp
$Comp
L schematicProject-rescue:GND-omega-dock-new-cache #PWR?
U 1 1 5BCD4A13
P 9700 3100
AR Path="/5B95942F/5BCD4A13" Ref="#PWR?"  Part="1" 
AR Path="/5BB736EF/5BCD4A13" Ref="#PWR?"  Part="1" 
AR Path="/5BB72E68/5BCD4A13" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9700 2850 50  0001 C CNN
F 1 "GND" V 9705 2972 50  0000 R CNN
F 2 "" H 9700 3100 50  0001 C CNN
F 3 "" H 9700 3100 50  0001 C CNN
	1    9700 3100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9250 3100 9300 3100
Wire Wire Line
	9600 3100 9700 3100
Text Label 8650 3100 0    50   ~ 0
LED_FMC
Wire Wire Line
	8650 3100 8950 3100
Wire Wire Line
	13050 4650 13400 4650
Wire Wire Line
	13050 4750 13400 4750
Wire Wire Line
	15550 5250 15200 5250
Wire Wire Line
	13050 4250 13400 4250
Wire Wire Line
	13050 4350 13400 4350
Wire Wire Line
	13050 4450 13400 4450
Wire Wire Line
	13050 4550 13400 4550
Wire Wire Line
	13050 4850 13400 4850
Wire Wire Line
	13050 4950 13400 4950
Wire Wire Line
	13050 5050 13400 5050
Text Label 15550 5250 2    50   ~ 0
FMC_A20
Text Label 13050 4250 0    50   ~ 0
FMC_D0
Text Label 13050 4350 0    50   ~ 0
FMC_D1
Text Label 13050 4450 0    50   ~ 0
FMC_D2
Text Label 13050 4550 0    50   ~ 0
FMC_D3
Text Label 13050 4750 0    50   ~ 0
FMC_D4
Text Label 13050 4650 0    50   ~ 0
FMC_D5
Text Label 13050 4850 0    50   ~ 0
FMC_D5
Text Label 13050 4950 0    50   ~ 0
FMC_D6
Text Label 13050 5050 0    50   ~ 0
FMC_D7
$Comp
L ICE40HX1K-TQ144:ICE40HX1K-TQ144 U?
U 1 1 5BB49B57
P 15600 4650
AR Path="/5B95942F/5BB49B57" Ref="U?"  Part="1" 
AR Path="/5BB736EF/5BB49B57" Ref="U?"  Part="1" 
AR Path="/5BB72E68/5BB49B57" Ref="U?"  Part="1" 
F 0 "U?" H 15700 7138 50  0000 C CNN
F 1 "ICE40HX1K-TQ144" H 15700 7047 50  0000 C CNN
F 2 "ICE40HX1K-TQ144:QFP50P2200X2200X160-144N" H 15600 4650 50  0001 L BNN
F 3 "None" H 15600 4650 50  0001 L BNN
F 4 "Lattice Semiconductor" H 15600 4650 50  0001 L BNN "Field4"
F 5 "TQFP-144 Lattice Semiconductor" H 15600 4650 50  0001 L BNN "Field5"
F 6 "Unavailable" H 15600 4650 50  0001 L BNN "Field6"
F 7 "HX Series 1280 LUTs 95 I/O 64 kBit RAM 1.2 V Surface Mount FPGA - TQFP-144" H 15600 4650 50  0001 L BNN "Field7"
F 8 "ICE40HX1K-TQ144" H 15600 4650 50  0001 L BNN "Field8"
	1    15600 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	13050 5150 13400 5150
Wire Wire Line
	13050 5250 13400 5250
Text Label 13050 5150 0    50   ~ 0
FMC_D8
Text Label 13050 5250 0    50   ~ 0
FMC_D9
Wire Wire Line
	15550 4350 15200 4350
Wire Wire Line
	15550 4750 15200 4750
Wire Wire Line
	15550 4650 15200 4650
Wire Wire Line
	15550 4550 15200 4550
Wire Wire Line
	15550 4450 15200 4450
Text Label 15550 4750 2    50   ~ 0
FMC_D15
Text Label 15550 4650 2    50   ~ 0
FMC_D14
Text Label 15550 4550 2    50   ~ 0
FMC_D13
Text Label 15550 4450 2    50   ~ 0
FMC_D11
Text Label 15550 4350 2    50   ~ 0
FMC_D10
Wire Wire Line
	15550 5150 15200 5150
Text Label 15550 5150 2    50   ~ 0
FMC_A19
Wire Wire Line
	15550 5050 15200 5050
Wire Wire Line
	15550 4950 15200 4950
Wire Wire Line
	15550 4850 15200 4850
Text Label 15550 5050 2    50   ~ 0
FMC_A18
Text Label 15550 4950 2    50   ~ 0
FMC_A17
Text Label 15550 4850 2    50   ~ 0
FMC_A16
Wire Wire Line
	15650 5950 16000 5950
Text Label 15650 5950 0    50   ~ 0
FMC_A21
Wire Wire Line
	15650 6050 16000 6050
Wire Wire Line
	15650 6150 16000 6150
Text Label 15650 6050 0    50   ~ 0
FMC_A22
Text Label 15650 6150 0    50   ~ 0
FMC_A23
Text HLabel 16000 6250 0    50   Input ~ 0
FCM_CLK
Text HLabel 16000 6350 0    50   Input ~ 0
FCM_NOE
Text HLabel 16000 6450 0    50   Input ~ 0
FCM_NWE
Text HLabel 16000 6550 0    50   Input ~ 0
FCM_NWAIT
Text HLabel 16000 6650 0    50   Input ~ 0
FCM_NE1
Text HLabel 16000 6750 0    50   Input ~ 0
FCM_NADV
Text HLabel 16000 6850 0    50   Input ~ 0
FCM_NBL0
Text HLabel 16000 6950 0    50   Input ~ 0
FCM_NBL1
Wire Wire Line
	15700 7050 16000 7050
Text Label 15700 7050 0    50   ~ 0
LED_FCM
Wire Bus Line
	8750 4250 8750 5200
Wire Bus Line
	9600 3850 9600 5600
$EndSCHEMATC
