# List of all the board related files.
BOARDSRC = $(CHIBIOS)/os/hal/boards/COLUMBA/board.c

# Required include directories
BOARDINC = $(CHIBIOS)/os/hal/boards/COLUMBA

# Shared variables
ALLCSRC += $(BOARDSRC)
ALLINC  += $(BOARDINC)
